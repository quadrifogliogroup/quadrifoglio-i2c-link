#include <stdint.h>
#include <stdio.h>

#include <unistd.h>				//Needed for I2C port
#include <fcntl.h>				//Needed for I2C port
#include <sys/ioctl.h>			//Needed for I2C port
#include <linux/i2c-dev.h>		//Real i2c-dev, needed for i2c port

#include "quadrifoglio_i2c_link/Transcutale_I2C_Master.hpp"


//int checksumFails = 0;

int OpenI2CMaster(int slaveaddr){
	//----------------------- OPEN THE I2C BUS -------------------------
	const char* filename = "/dev/i2c-1";
	int file_i2c;
	
	if ((file_i2c = open(filename, O_RDWR)) < 0)
	{
		//ERROR HANDLING: you can check errno to see what went wrong
		printf("Failed to open the i2c bus.\n");
		return 1;
	}
	
	if (ioctl(file_i2c, I2C_SLAVE, slaveaddr) < 0)
	{
		printf("Failed to acquire bus access and/or talk to slave.\n");
		//ERROR HANDLING; you can check errno to see what went wrong
		return 1;
	}
	
	return file_i2c;
}

