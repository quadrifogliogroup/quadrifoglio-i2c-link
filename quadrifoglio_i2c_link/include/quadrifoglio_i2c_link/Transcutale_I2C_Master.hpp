#include <stdint.h>
#include <stdio.h>

#include <unistd.h>				//Needed for I2C port
#include <fcntl.h>				//Needed for I2C port
#include <sys/ioctl.h>			//Needed for I2C port
#include <linux/i2c-dev.h>		//Real i2c-dev, needed for i2c port

#ifndef Transcutale_I2C_Master_h
#define Transcutale_I2C_Master_h


//int checksumFails = 0;


int OpenI2CMaster(int slaveaddr);

// The following functions are defined here for now, until I figure out
// a better way to deal with the templates.

template <class T> int GetAnything(int fd, uint8_t cmd, T& value){
	static int checksumFails = 0;
    
    T tempStruct;
    uint8_t* p = (uint8_t*)(void*)&tempStruct;
    
    i2c_smbus_read_i2c_block_data(fd, cmd, sizeof(tempStruct), p);
    
    if (CheckDaSum(tempStruct)){
		value = tempStruct;
	}

    else {
		checksumFails++;
	}
	//value = tempStruct;
	return checksumFails;
}

template <class T> void SendAnything(int fd, uint8_t cmd, T& value){
    CheckDaSum(value);
    uint8_t* p = (uint8_t*)(void*)&value;
    
    i2c_smbus_write_i2c_block_data(fd, cmd, sizeof(value), p);
}

/*Takes a block of memory, calculates a XOR checksum of all exept the
 * last byte, and puts it into the last byte.
 */
template <class T> bool CheckDaSum(const T& value){ 
	uint8_t* p = (uint8_t*)(void*)&value;         //Get pointer to beginning of input memory
	
	uint8_t checksum = 0;
	for(uint8_t i = 0; i < sizeof(value)-1; i++){ //Iterate through input memory, except for last byte
		checksum ^= *p++;                           //XOR everything together and increment
	}
	bool passed = *p == checksum;                   //Pointer p is now pointing to the original checksum, which is not yet overridden
	
	*p = checksum;                                  //Put the 1 byte checksum into the last memory byte
	return passed;
}

#endif
