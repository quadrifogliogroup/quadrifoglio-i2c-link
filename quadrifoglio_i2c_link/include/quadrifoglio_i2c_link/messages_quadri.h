//Message maximum size is 32 bytes

//=========MESSAGES FROM RASPI TO MAIN UC==========

//===== Raspi -> Uc ===== Contains basic drive commands ======
struct __attribute__((packed, aligned(1))) CommandMsg{ 
  uint32_t raspitime; //Time from raspi, used as a kind of heartbeat signal
  
  uint16_t frontLeftSrv;  //Servo command is between 1000 and 5000. 3000 is center position.
  uint16_t frontRightSrv;
  uint16_t rearLeftSrv;
  uint16_t rearRightSrv; //8 bytes
  uint16_t frontLeftMot;   //Motor command is +- 255 with 255 offset, so 0-510
  uint16_t frontRightMot;
  uint16_t rearLeftMot;
  uint16_t rearRightMot; //16 bytes
  uint8_t commandFlags; //Command flags containing : | driveArm | rcMode | closedLoop |  |  |  |  |  |
  uint8_t checksum;    //Do not write to this, comms fuctions do that automatically  
  //22 bytes
};

//=========MESSAGES FROM MAIN UC TO RASPI==========
//===== Uc -> Raspi ===== Contains basic drive commands ======
struct __attribute__((packed, aligned(1))) FeedbackMsg{ 
  uint32_t uptime;      //Microcontroller uptime in milliseconds, used to detect crashes
  
  uint16_t frontLeftEncoder;  //Accumulated encoder value since last send, 32768 offset (zero is 32768) 
  uint16_t frontRightEncoder;
  uint16_t rearLeftEncoder;
  uint16_t rearRightEncoder; 
  uint16_t motorCurrent;
  uint16_t raspiCurrent;
  uint16_t servoCurrent;
  uint16_t ucCurrent;
  uint16_t batteryVoltage;
  
  uint8_t stickRY_h; //Radio stick positions, first 8 bytes
  uint8_t stickRX_h;
  uint8_t stickLY_h;
  uint8_t stickLX_h;
  uint8_t sticks_l; //Low bytes of sticks in order above for increased resolution
  uint8_t switches1; //Status bits of radio switches
  uint8_t switches2;
  
  uint8_t statusFlags; //Command flags containing : | driveArm | rcMode | closedLoop |  |  |  |  |  |
  uint8_t checksum; //Do not write to this, comms fuctions do that automatically
  //31 bytes total
};

//=========MESSAGES FROM RASPI TO SATELLITE UC==========

struct __attribute__((packed, aligned(1))) LedMessage{
  uint8_t mode; //This tells what the leds should do. 0-Off 1-SpeedMode 2-CurrentMode
  uint8_t modeData; //This is some mode-specific information (speed in speed mode, current in current mode and so on
};

//=========MESSAGES FROM SATELLITE UC TO RASPI==========

struct __attribute__((packed, aligned(1))) SonarMessage{
  uint8_t which; //This tells from which ultrasound sensor the measurement was taken
  uint16_t meas; //This is the actual distance measurement in cm
  uint32_t chronitron;  //WHEN (according to the uc's clock) this measurement was taken, in milliseconds after boot
  uint16_t servo;
};
